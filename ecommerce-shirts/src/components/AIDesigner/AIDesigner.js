import React, { useState } from 'react';
import './AIDesigner.css';

function AIDesigner() {
    const [input, setInput] = useState('');
    const [design, setDesign] = useState(null);
    const [loading, setLoading] = useState(false);

    const handleInputChange = (event) => {
        setInput(event.target.value);
    };

    const handleSubmit = async () => {
        setLoading(true); // Indicate loading state
        try {
            // Simulate an API call with a timeout
            const imageUrl = await fetchDesign(input);
            setDesign(imageUrl);
        } catch (error) {
            console.error("Failed to generate design:", error);
            setDesign("Error generating design. Please try again.");
        } finally {
            setLoading(false); // Reset loading state
        }
    };

    // Mock function to simulate fetching an image URL from an API
    function fetchDesign(input) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (input) { // Simulate successful API response
                    resolve("https://cdn.midjourney.com/3bfd54e6-a71f-4343-afb6-c15b87360542/0_0.webp");
                    //resolve(`https://via.placeholder.com/500x300.png?text=Design+for+${encodeURIComponent(input)}`);
                } else {
                    reject("No input provided");
                }
            }, 2000); // Simulate network delay
        });
    }

    return (
        <div className="ai-designer">
            <h1>AI T-Shirt Designer</h1>
            <p className="description">
                Welcome to our AI T-Shirt Designer! Enter your preferences and click 'Generate Design' to see a unique design created by our AI.
            </p>
            <input
                type="text"
                value={input}
                onChange={handleInputChange}
                placeholder="Type your design preferences (e.g., colors, motifs)"
            />
            <button onClick={handleSubmit} disabled={loading}>
                {loading ? 'Generating...' : 'Generate Design'}
            </button>
            {design && <div className="design-output">
                {typeof design === 'string' && design.startsWith('Error') ? 
                    <p>{design}</p> : 
                    <img src={design} alt="Generated design" />
                }
            </div>}
        </div>
    );
}

export default AIDesigner;
