import React from 'react';
import './DesignCard.css'; // Ensure the path to your CSS file is correct
import { Link } from 'react-router-dom';


function DesignCard({ design }) {
  return (
    <Link to={`/design/${design.id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
      <div className="design-card">
        <img src={design.designImageUrl} alt={design.name} className="design-image" />
        <div className="card-content">
          <h3>{design.name}</h3>
          <p className="description">{design.description}</p>
          <p className="price">{`${design.price}€`}</p>
        </div>
      </div>
    </Link>
  );
}

export default DesignCard;