import React from 'react';
import './About.css'; 

function About() {
    return (
        <div className="about">
            <h1>About IMAGINE-TEE</h1>
            <p>
                Welcome to ImagineTee, where creativity meets fashion. We are dedicated to providing you
                with unique and personalized t-shirts designed by both our talented in-house artists and
                through our innovative AI Designer tool. Our mission is to help you express your individuality
                through our apparel.
            </p>
            <p>
                At ImagineTee, we believe in high-quality products, ethical production practices, and exceptional
                customer service. We're thrilled to share our passion for creativity and fashion with you!
            </p>
            <p>
                Connect with us on Instagram! Follow <a href="https://www.instagram.com/imaginetee" target="_blank" rel="noopener noreferrer">@ImagineTee</a> to stay up to date with the latest designs, behind-the-scenes looks, and special promotions. Don't miss out on the conversation with our creative community.
            </p>
            <p>
                Have any questions? Reach out to us at <a href="mailto:contact@imaginetee.com" target="_blank" rel="noopener noreferrer">contact@imaginetee.com</a>. Whether it's feedback, a question about your order, or just to say hi, we're always here to listen and help.
            </p>
        </div>
    );
}

export default About;
