// DesignsGallery.js
import React from 'react';
import DesignCard from '../DesignCard/DesignCard';
import './DesignsGallery.css'; // Make sure to create a corresponding CSS file for styling

function DesignsGallery({ designs }) {
  const exclusiveTees = designs.filter(design => design.isExclusive);
  const regularTees = designs.filter(design => !design.isExclusive);

  return (
    <div className="designs-gallery">
      <div className="section exclusive-tees">
        <h2 className="section-title">EXCLUSIVE TEES</h2>
        <div className="cards-container">
          {exclusiveTees.map(design => (
            <DesignCard key={design.id} design={design} />
          ))}
        </div>
      </div>

      <div className="section tee-shop">
        <h2 className="section-title">TEE SHOP</h2>
        <div className="cards-container">
          {regularTees.map(design => (
            <DesignCard key={design.id} design={design} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default DesignsGallery;
