import React from 'react';
import './Offers.css'; // Make sure to add some styles

function InstagramOffer() {
    return (
        <div className="instagram-offer">
            <h1>Share Your Story, Get a Discount!</h1>
            <p>
                Love our products? Share a story of our webpage or your favorite products on Instagram
                and tag us <strong>@ImagineTee</strong> along with the hashtag <strong>#ImagineTeeLoves</strong>.
                Once we see it, we'll send you a 15% discount code for your next purchase!
            </p>
            <button onClick={() => navigator.clipboard.writeText('#ImagineTeeLoves @ImagineTee')}>
                Copy Hashtag and Tag
            </button>
            <p>Click the button above to copy the required hashtag and our tag to your clipboard.</p>
        </div>
    );
}

export default InstagramOffer;
