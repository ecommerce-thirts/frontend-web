import React, { useState, useEffect } from 'react';

function FooterBanner() {
  // Initialize the visibility state based on local storage
  const [isVisible, setIsVisible] = useState(() => {
    return localStorage.getItem('bannerVisible') !== 'false';
  });

  // Effect to set local storage when visibility changes
  useEffect(() => {
    localStorage.setItem('bannerVisible', isVisible);
  }, [isVisible]);

  if (!isVisible) return null;

  return (
    <div style={styles.banner}>
      <p style={styles.text}>Discover the power of creativity with our AI! Craft your unique design now - find it at the top of the page!</p>
      <button style={styles.button} onClick={() => setIsVisible(false)}>
        Close
      </button>
    </div>
  );
}

const styles = {
    banner: {
      position: 'fixed',
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: '#FFD54F', // Soft mustard yellow
      color: '#333', // Dark text for better readability
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      padding: '0px 5px',
      boxSizing: 'border-box',
      zIndex: 1000,
      fontFamily: 'Josefin Sans, sans-serif', // Applying font globally to the banner
    },
    text: {
      marginRight: '10px',
      fontFamily: 'Josefin Sans, sans-serif', // Specifically for the text
    },
    button: {
      backgroundColor: '#F57C00', // A darker shade of orange
      color: 'white',
      border: 'none',
      borderRadius: '5px',
      padding: '0px 5px',
      cursor: 'pointer',
      fontWeight: 'bold',
      outline: 'none',
      fontFamily: 'Josefin Sans, sans-serif', // Consistency in font for the button
    }
};

export default FooterBanner;
