import React from 'react';
import './Header.css';
import logoImage from '../../images/imagineteelogo.png';
import shoppingCartImage from '../../images/shopping_cart.png';


function Header() {
    return (
        <header className="header">
            <div className="branding">
                <a href="/" className="home-link" aria-label="Back to homepage">
                    <img src={logoImage} alt="ImagineTee Logo" className="logo-image" />
                </a>
            </div>
            <div className="additional-links">
                <a href="/offers" aria-label="View special offers">Offers</a>
                <a href="/ai" aria-label="Let AI create your design">AI</a>
                <a href="/contact" aria-label="Learn more about us">Contact</a>
            </div>
            <div className="cart">
                <a href="/cart" aria-label="View your shopping cart">
                    <img src={shoppingCartImage} alt="Shopping Cart" className="shopping-cart-image" />
                </a>
            </div>
        </header>
    );
}

export default Header;
