import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import './DesignDetail.css'; 

function DesignDetail() {
  const { id } = useParams();
  const [design, setDesign] = useState(null);

  useEffect(() => {
    fetchDesignById(id).then(setDesign);
  }, [id]);

  if (!design) {
    return <p>Loading...</p>;
  }
  return (
    <div className="design-detail">
      <div className="header-container">
        <h1>{design.name}</h1>
        <p className="artist-info">Designed by <em>{design.artist}</em></p>
        <p className="price">Price: {design.price}€</p>
      </div>
      <div className="images-container">
        <img src={design.designImageUrl} alt={`${design.name} design`} />
        <img src={design.modelImageUrl} alt={`${design.name} on model`} />
      </div>
      <button className="add-to-cart-button">Add to Cart</button>
    </div>
  );
  
}

  
  async function fetchDesignById(id) {
    // Replace this with your actual API call
    return {
      id: id,
      name: 'Sample Design',
      description: 'This is a detailed description of the design.',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1708513243-214079-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1708513243-214079-womens-500x600.jpg',
      price: '25',
      artist: 'ImagineTee'
    };
  }
  
  export default DesignDetail;
