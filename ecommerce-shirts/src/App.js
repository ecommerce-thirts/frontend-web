import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import Header from './components/Header/Header';
import DesignsGallery from './components/DesignsGallery/DesignsGallery';
import About from './components/About/About'; // Import the About component
import DesignDetail from './components/DesignDetail/DesignDetail'; // Import the new component
import AIDesigner from './components/AIDesigner/AIDesigner'; // Import the AI Designer component
import Offers from './components/Offers/Offers'; // Import the AI Designer component
import FooterBanner from './components/FooterBanner/FooterBanner'; // Import the AI Designer component



function App() {
  // Sample data for the designs
  const designs = [
    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1708513243-214079-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1708513243-214079-zoom-500x600.jpg',
      price: '50',
      artist: 'ImagineTee',
      isExclusive: true
    },
    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437320-217175-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437320-217175-mens-500x600.jpg',
      price: '50',
      artist: 'ImagineTee',
      isExclusive: true
    },
    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437287-216868-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437287-216868-mens-500x600.jpg',
      price: '50',
      artist: 'ImagineTee',
      isExclusive: true
    },
    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437360-217250-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437360-217250-zoom-500x600.jpg',
      price: '50',
      artist: 'ImagineTee',
      isExclusive: true
    },
    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437376-217233-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437376-217233-zoom-500x600.jpg',
      price: '50',
      artist: 'ImagineTee',
      isExclusive: true
    },
    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1711722610-215891-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437376-217233-zoom-500x600.jpg',
      price: '50',
      artist: 'ImagineTee',
      isExclusive: true
    },
    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1711722561-215426-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437376-217233-zoom-500x600.jpg',
      price: '50',
      artist: 'ImagineTee',
      isExclusive: true
    },
    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1697615427-190384-zoom-500x600.jpg',
      modelImageUrl: 'https://cdn.qwertee.com/images/designs/product-thumbs/1713437376-217233-zoom-500x600.jpg',
      price: '50',
      artist: 'ImagineTee',
      isExclusive: true
    },


    {
      id: 1,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.midjourney.com/93647d07-882e-4420-8142-4b53de4f052a/0_0.webp',
      modelImageUrl: 'https://cdn.midjourney.com/594a5cb5-d6d6-4672-8319-6be139ce4ea2/0_0.webp',
      price: '50',
      artist: 'ImagineTee',
    },
    {
      id: 2,
      name: 'Urban Trend',
      description: 'Reflect your contemporary spirit',
      designImageUrl: 'https://cdn.midjourney.com/426fbad4-b3e8-4142-85b9-ece92dcf63ad/0_0.webp',
      modelImageUrl: 'https://cdn.midjourney.com/594a5cb5-d6d6-4672-8319-6be139ce4ea2/0_0.webp',
      price: '50',
      artist: 'ImagineTee',
    },
    {
      id: 3,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.midjourney.com/7561e48c-533a-4bf8-97e5-3e09dbb87e24/0_3.webp',
      modelImageUrl: 'https://cdn.midjourney.com/594a5cb5-d6d6-4672-8319-6be139ce4ea2/0_0.webp',
      price: '50',
      artist: 'ImagineTee',
    },
    {
      id: 4,
      name: 'Urban Trend',
      description: 'Reflect your contemporary spirit',
      designImageUrl: 'https://cdn.midjourney.com/3bfd54e6-a71f-4343-afb6-c15b87360542/0_0.webp',
      modelImageUrl: 'https://cdn.midjourney.com/594a5cb5-d6d6-4672-8319-6be139ce4ea2/0_0.webp',
      price: '25',
      artist: 'ImagineTee',
    },
    {
      id: 5,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.midjourney.com/6104dcb7-7b8e-41ac-bbbb-c78c6e587434/0_0.webp',
      modelImageUrl: 'https://cdn.midjourney.com/594a5cb5-d6d6-4672-8319-6be139ce4ea2/0_0.webp',
      price: '25',
      artist: 'ImagineTee',
    },
    {
      id: 6,
      name: 'Urban Trend',
      description: 'Reflect your contemporary spirit',
      designImageUrl: 'https://cdn.midjourney.com/f9b23a33-16f7-445b-b631-d0a35e80f6d1/0_0.webp',
      modelImageUrl: 'https://cdn.midjourney.com/594a5cb5-d6d6-4672-8319-6be139ce4ea2/0_0.webp',
      price: '25',
      artist: 'ImagineTee'
    },
    {
      id: 7,
      name: 'Eternal Style',
      description: 'An evergreen design for every wardrobe',
      designImageUrl: 'https://cdn.midjourney.com/33672e3d-3c1a-4b67-acf4-4fa792acb589/0_1.webp',
      modelImageUrl: 'https://cdn.midjourney.com/594a5cb5-d6d6-4672-8319-6be139ce4ea2/0_0.webp',
      price: '25',
      artist: 'ImagineTee'
    }
  ];

  return (
    <Router>
      <div className="App">
        <Header />
        <Routes>
          <Route path="/contact" element={<About />} />
          <Route path="/" element={<DesignsGallery designs={designs} />} />
          <Route path="/design/:id" element={<DesignDetail />} /> 
          <Route path="/ai" element={<AIDesigner />} />
          <Route path="/offers" element={<Offers />} />
        </Routes>
      </div>
      <div>
      <FooterBanner />
      {/* Other components of your app */}
    </div>
    </Router>
  );
}

export default App;
